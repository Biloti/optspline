CC = gcc
CFLAGS = -O2 -g -funroll-loops \
	-Wall -Wbad-function-cast -Wformat -Wparentheses \
	-Wsign-compare -Wmissing-prototypes
HTMLDOCDIR = html

# If ATLAS is available
#ATLASDIR=/usr/lib/sse2
#IDIR = -L$(ATLASDIR)
#LIBS = -lgsl -lcblas -latlas -lm

# If ATLAS is not available
ATLASDIR=
IDIR = 
LIBS = -lgsl -lgslcblas -lm

OBJS = optsp.o optspline.o splined.o cmdline.o

all: optsp

optsp: $(OBJS)
	$(CC) $(CFLAGS) $(IDIR) $(OBJS) $(LIBS) -o optsp

optsp-static: $(OBJS)
	$(CC) $(CFLAGS) -static $(IDIR) $(OBJS) $(LIBS) -o optsp-static

optspline.o: optspline.c optspline.h
	$(CC) -c $(CFLAGS) $(IDIR) optspline.c

optsp.o: optsp.c cmdline.c
	$(CC) -c $(CFLAGS) $(IDIR) optsp.c

splined.o: splined.c
	$(CC) -c $(CFLAGS) $(IDIR) splined.c

cmdline.o: cmdline.c
	$(CC) -c $(CFLAGS) cmdline.c

cmdline.c: optspline.ggo
	gengetopt < optspline.ggo --file-name=cmdline

install: optsp
	cp optsp ~/bin

doc:
	mkdir -p ndproj html && naturaldocs -i . -p ndproj/ -o FramedHtml $(HTMLDOCDIR)

clean:
	rm -f *.o *'~' optsp optsp-static smoothed.dat knots.dat

teste1: optsp
	./xoptsp -d test/teste1.dat -n 10

teste2: optsp
	./xoptsp -d test/teste2.dat -n 9

teste3: optsp
	./xoptsp -d test/teste3.dat -n 21

teste4: optsp
	./xoptsp -d test/teste4.dat

teste5: optsp
	./xoptsp -d test/teste5.dat

teste6a: optsp
	./xoptsp -d test/teste6a.dat -n 31

teste6b: optsp
	./xoptsp -d test/teste6b.dat -n 43
