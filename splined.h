/* $Id: splined.h,v 1.2 2004/12/08 12:51:16 biloti Exp $ */

int spline(int *n, double *x, double *y, double *b,
	   double *c__, double *d__);

double seval(int *n, double *u, double *x, double *y,
	     double *b, double *c__, double *d__);

int sgheval(int *n, double *u, double *x, double *y,
	   double *b, double *c__, double *d__,
	    double *s, double *g, double *h);
