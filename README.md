# Optspline #

This code implements the optimal-spline smoother.

Given a set of points on the Euclidian plane, optsp fits a cubic
spline, with a prescribed number of knots, in the least-square sense.

Smoother values, over the same horizontal coordinates, are produced
and sent to stdout. Optionally, a file can be prescribed to store the
optimal knots found.

`gengetopt` tool is required to parse command-line options.

### References ###

* R. Biloti, L. T. Santos and M. Tygel. Automatic smoothing by optimal splines, *Revista Brasileira da Geofísica*, 21(2), pp. 173-178, 2005.

* J. Schleicher and R. Biloti. A frequency criterion for optimal node selection in smoothing with cubic splines, *Geophysical Prospecting*, 56, pp. 229-237, 2008. 


Ricardo Biloti <biloti@unicamp.br>, Computational Geophysics Group, DMA -- IMECC -- UNICAMP
